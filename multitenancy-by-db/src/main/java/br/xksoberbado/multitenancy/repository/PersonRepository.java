package br.xksoberbado.multitenancy.repository;

import br.xksoberbado.multitenancy.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;


public interface PersonRepository extends JpaRepository<Person, Long> {
}
