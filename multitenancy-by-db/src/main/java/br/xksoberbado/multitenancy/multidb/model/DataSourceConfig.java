package br.xksoberbado.multitenancy.multidb.model;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "DATA_SOURCE_CONFIGS")
public class DataSourceConfig {

    @Id
    private String tenant;

    @Column(name = "DRIVER_CLASS_NAME")
    private String driverClassName;

    private String url;

    private String username;

    private String password;
}
