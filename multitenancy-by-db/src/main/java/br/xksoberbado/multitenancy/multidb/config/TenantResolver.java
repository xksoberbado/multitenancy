package br.xksoberbado.multitenancy.multidb.config;

import lombok.extern.java.Log;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.stereotype.Component;

//NOTE: é quem diz para o Hibernate qual a tenant que será usada para abrir a sessão

@Log
@Component
public class TenantResolver implements CurrentTenantIdentifierResolver {

    @Override
    public String resolveCurrentTenantIdentifier() {
        String tenant = TenantThreadLocal.getTenant()
                .orElse(Tenants.TENANT_DEFAULT);

        log.info("** Tenant: "+ tenant);

        return tenant;
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true; //NOTE: isso diz que qualquer sessão aberta deve ter um identificador/tenant
    }
}
