//package br.xksoberbado.multitenancy.multidb.service;
//
//import br.xksoberbado.multitenancy.multidb.model.DataSourceConfig;
//
//import java.util.List;
//import java.util.Optional;
//
//public interface DataSourceConfigService {
//
//    Optional<DataSourceConfig> findByTenant(String tenant);
//
//    List<DataSourceConfig> findAll();
//
//}
